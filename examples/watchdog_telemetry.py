# This is auto-generated code from the source Pluto file.
from pluto_engine.language import *


class Procedure_(Procedure):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.main_body.append(stmt_pos_16)
        self.watchdog_body.append(stmt_pos_520)
        self.confirmation.append(stmt_pos_1041)


def stmt_pos_56(caller):
    caller.log(lambda x: "Main: switch on AOCS")


def stmt_pos_92(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    activity_call = ActivityCall(DummySatellite.AOCS.SwitchON, arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_157(caller):
    caller.log(lambda x: DummySatellite.AOCS.Mode.get_value())


def stmt_pos_202(caller):
    caller.log(lambda x: "Main: switch off AOCS")


def stmt_pos_239(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    activity_call = ActivityCall(DummySatellite.AOCS.SwitchOFF, arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_305(caller):
    caller.log(lambda x: DummySatellite.AOCS.Mode.get_value())


def stmt_pos_441(caller):
    caller.wait_for_relative_time(lambda x: ureg('2s'))


def stmt_pos_463(caller):
    caller.log(lambda x: "main body end")


def stmt_pos_16(caller):
    step = Step_stmt_pos_16(caller)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_pos_16(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.main_body.append(stmt_pos_56)
        self.main_body.append(stmt_pos_92)
        self.main_body.append(stmt_pos_157)
        self.main_body.append(stmt_pos_202)
        self.main_body.append(stmt_pos_239)
        self.main_body.append(stmt_pos_305)
        self.main_body.append(stmt_pos_441)
        self.main_body.append(stmt_pos_463)


def stmt_pos_602(caller):
    caller.wait_until_expression(lambda x: DummySatellite.AOCS.Mode.get_value() == "OFF", timeout=None)


def stmt_pos_715(caller):
    caller.log(lambda x: "Watchdog: detected AOCS is off!")


def stmt_pos_770(caller):
    caller.log(lambda x: "Watchdog: switch on AOCS now")


def stmt_pos_822(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    activity_call = ActivityCall(DummySatellite.AOCS.SwitchON, arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_895(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))


def stmt_pos_924(caller):
    caller.log(lambda x: DummySatellite.AOCS.Mode.get_value())


def stmt_pos_520(caller):
    step = Step_stmt_pos_520(caller)
    caller.watchdogs['Step_stmt_pos_520'] = step
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_pos_520(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.preconditions.append(stmt_pos_602)
        self.main_body.append(stmt_pos_715)
        self.main_body.append(stmt_pos_770)
        self.main_body.append(stmt_pos_822)
        self.main_body.append(stmt_pos_895)
        self.main_body.append(stmt_pos_924)


def stmt_pos_1041(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))
